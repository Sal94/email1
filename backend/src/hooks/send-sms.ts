// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
  
  const { result } = context;
  // PUT YOUR SAFE EMAIL RECIPIENTS HERE
  const safeRecipients = [ "+16047040345", "+16044414476" ];
  
  if ( !safeRecipients.includes(result['to'])) {
    console.log( "BAD: " + result['to']);
    return context;
  }
  console.log( "GOOD: " + result['to']);
    
  // Download the helper library from https://www.twilio.com/docs/node/install
  // Your Account Sid and Auth Token from twilio.com/console
  // DANGER! This is insecure. See http://twil.io/secure
  const accountSid = 'AC1057cc8ad3f4748289d2b1b1afe3bba9';
  const authToken = '9db7379e5e5927041d7f1d764b1dbfe0';
  const client = require('twilio')(accountSid, authToken);

  client.messages
        .create({body: 'Hi there!', from: '+16046707181', to: result['to']})
        .then(message => {
            return console.log(message.sid);
        });
    

    return context;
  };
}
