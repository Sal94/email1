// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import nodemailer from 'nodemailer';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { result } = context;
     // PUT YOUR SAFE EMAIL RECIPIENTS HERE
    const safeRecipients = ["sdesimone00@mylangara.ca", "desimonesalvatore94@gmail.com"];
    
    if ( !safeRecipients.includes(result['to'])) {
      console.log( "BAD: " + result['to']);
      return context;
    }
    console.log( "GOOD: " + result['to']);
    
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: "smtp.sendgrid.net",
      port: 587,
      secure: false,
      requireTLS: true,
      auth: {
        user: "apikey", // SendGrid user
        pass: "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" // SendGrid password
      }
    });
    
    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: '"No Reply" <noreply@494912.xyz>', // PUT YOUR DOMAIN HERE
      to: result.to, // list of receivers
      subject: "Hello " + result.id, // Subject line
      text: "Your id is: " + result.id // plain text body
    });
    console.log("Message sent: %s", info.messageId);
    
    return context;
  };
}
